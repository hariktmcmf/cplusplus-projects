all:
	cd Hangman && make
	cd HeapPackage && make
	cd InfectionSimulation && make
	cd LinkedList && make
	cd MazeSolver && make
	cd Pokedex && make
	cd Pokedex_BST && make
	cd SortCompare && make
	cd SpaceBattle && make
	cd TheatreSimulation && make
	
	